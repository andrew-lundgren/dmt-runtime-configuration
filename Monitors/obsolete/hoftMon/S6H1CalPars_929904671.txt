#
#  h(t) calibration parameters from Xavi 7/6/2009
#  These parameters match the filter file in the calibration cvs at:
#  calibration/timedomain/runs/S6/H1/V0/S6H1Filters_929904671.txt
#
 --olg-re 0.0312699045558972 --olg-im 0.213998181589933
 --whitener-re 0.0099845355484356 --whitener-im -0.000556250270852907
 --cal-line-freq 1144.3
