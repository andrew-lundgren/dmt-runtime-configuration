#
#   ER3_hoft_DQ_flags.osc  contains the flags in the LLD data quality vector
#
#   Change list:
#   Date        Author      Change
#   2012-12-02  J. Zweizig  Remove H2 flags.
#   2014-11-28  J. Zweizig  New flags from gstlal_compute_strain
#
# H1:hoft_Science    bitand "H1:LLD-DQ_VECTOR" mask=0x01 fraction=1.0
# H1:hoft_Up         bitand "H1:LLD-DQ_VECTOR" mask=0x02 fraction=1.0
# H1:hoft_Calibrated bitand "H1:LLD-DQ_VECTOR" mask=0x04 fraction=1.0
# H1:hoft_Ready      boolean "H1:hoft_Science & H1:hoft_Calibrated"
#
# L1:hoft_Science    bitand "L1:LLD-DQ_VECTOR" mask=0x01 fraction=1.0
# L1:hoft_Up         bitand "L1:LLD-DQ_VECTOR" mask=0x02 fraction=1.0
# L1:hoft_Calibrated bitand "L1:LLD-DQ_VECTOR" mask=0x04 fraction=1.0
# L1:hoft_Ready      boolean "L1:hoft_Science & L1:hoft_Calibrated"
#
#  Bits from ER6 gstlal_compute_strain
#
#  bit   Meaning
#   0    HOFT_OK
#   1    SCIENCE_LOCKED
#   2    LOCKED
#   3    HOFT_PROD
#   4    FILTERS_OK
#   5    GAMMA_OK
#
H1:hoft_Science    bitand "H1:GDS-CALIB_STATE_VECTOR" mask=0x02 fraction=1.0
H1:hoft_Up         bitand "H1:GDS-CALIB_STATE_VECTOR" mask=0x04 fraction=1.0
H1:hoft_Calibrated bitand "H1:GDS-CALIB_STATE_VECTOR" mask=0x01 fraction=1.0
H1:hoft_Ready      bitand "H1:GDS-CALIB_STATE_VECTOR" mask=0x03 fraction=1.0
#
L1:hoft_Science    bitand "L1:GDS-CALIB_STATE_VECTOR" mask=0x02 fraction=1.0
L1:hoft_Up         bitand "L1:GDS-CALIB_STATE_VECTOR" mask=0x04 fraction=1.0
L1:hoft_Calibrated bitand "L1:GDS-CALIB_STATE_VECTOR" mask=0x01 fraction=1.0
L1:hoft_Ready      bitand "L1:GDS-CALIB_STATE_VECTOR" mask=0x03 fraction=1.0
#
# Added for O2b 6/6/2017 by JGZ
V1:hoft_data_OK bitand "V1:DQ_ANALYSIS_STATE_VECTOR" mask=0x01 fraction=1.0
V1:hoft_Science bitand "V1:DQ_ANALYSIS_STATE_VECTOR" mask=0x02 fraction=1.0
#
# Flag for picking out when Calibration filters are bad: (bitnand fraction=0 
# means any sample where this bit is zero activates the segment for a second)
L1:DMT-CALIB_FILTER_NOT_OK_s bitnand "L1:GDS-CALIB_STATE_VECTOR" mask=0x10 fraction=0
# Flag for picking out when Calibration filters are bad: (bitnand fraction=0 
# means any sample where this bit is zero activates the segment for a second)
H1:DMT-CALIB_FILTER_NOT_OK_s bitnand "H1:GDS-CALIB_STATE_VECTOR" mask=0x10 fraction=0
# H1:DMT-DQ_VECTOR Channel Segments
H1:DMT_DQ_VECTOR_PARITY_OK_s testparity "H1:DMT-DQ_VECTOR" value=1
H1:DMT_DQ_VECTOR_BIT1_VETO_s bitnand "H1:DMT-DQ_VECTOR" mask=0x02 fraction=0
H1:DMT_DQ_VECTOR_BIT2_VETO_s bitnand "H1:DMT-DQ_VECTOR" mask=0x04 fraction=0
H1:DMT_DQ_VECTOR_BIT4_VETO_s bitnand "H1:DMT-DQ_VECTOR" mask=0x10 fraction=0
# L1:DMT-DQ_VECTOR Channel Segments
L1:DMT_DQ_VECTOR_PARITY_OK_s testparity "L1:DMT-DQ_VECTOR" value=1
L1:DMT_DQ_VECTOR_BIT1_VETO_s bitnand "L1:DMT-DQ_VECTOR" mask=0x02 fraction=0
L1:DMT_DQ_VECTOR_BIT2_VETO_s bitnand "L1:DMT-DQ_VECTOR" mask=0x04 fraction=0
L1:DMT_DQ_VECTOR_BIT4_VETO_s bitnand "L1:DMT-DQ_VECTOR" mask=0x10 fraction=0
#
#  Strain gate asserted
H1:GDS_STRAIN_GATED_s bitnand "H1:DMT-DQ_VECTOR_GATED" mask=0x08 fraction=0
L1:GDS_STRAIN_GATED_s bitnand "L1:DMT-DQ_VECTOR_GATED" mask=0x08 fraction=0
#
#  H1/L1 IDQ data OK
H1:IDQ_OK_OVL_10_2048_s valueabove "H1:IDQ-OK_OVL_10_2048" threshold=0 fraction=1
L1:IDQ_OK_OVL_10_2048_s valueabove "L1:IDQ-OK_OVL_10_2048" threshold=0 fraction=1
#
