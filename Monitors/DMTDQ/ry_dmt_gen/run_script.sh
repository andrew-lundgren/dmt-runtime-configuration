#!/bin/bash

smcreate LHO_Combine -nbuf 16 -lbuf 2000000 
smlatency LHO_Combine & # Kludge to make the shmbuffer stay alive
DMTGen -conf combine_data.cfg -realtime -debug 99
