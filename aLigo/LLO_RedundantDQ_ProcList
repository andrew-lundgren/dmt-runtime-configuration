#
#    File:	LLO_RedundantDQ_ProcList
#
#    Purpose:	This ProcList file is used by the procsetup utility to 
#		create the node-specific configuration files for the
#		process manager.
#
#    Author:	J. Zweizig
#
#    Version:	1.0; Modified January 15, 2015
#    		Version for ER7 running before aLigo
#    		3.0; Modified November 29, 2018
#    		Version for ER13/O3
#    		3.01; Modified Fecbruary 12, 2019
#    		Version for ER14/O3
#
#-
#
#    This file contains one configuration line for each process to be 
#    run. The configuration line may be continued over multiple text 
#    by ending a non-terminal line with a backslash (\). Comments start
#    with a hash-mark (#) and continue to the end of the line. The 
#    configuration line contains the following fields:
#
#    1) Process name
#    2) CPU usage
#    3) Command to be executed by the process
#    4) Node assignment constraints
#    5) Procmgr tag
#    6) Files used by the process
#
#    Each field should be enclosed in braces if it contains white-space 
#    characters. The command line may contain symbols or escape sequences
#    as allowed by the process manager. The "node assignment constraints" 
#    field is a perl expression that evaluates to true if the process may
#    be run on a given node. The symbols in this expression are substituted 
#    with values defined in the node list on a node-by-node basis.
#
#    Generate procmgt configure files from this file by running the 
#    following command:
#
#       procsetup --file:LLO_RedundantDQ
#
#==========================================================================
#
#=======================================  Infrastructure process definitions
#
#   Servers
#   Load unmeasured
#
NameServer 100	{dmt_nameserver } \
	  	{$comnode} \
	  	{-log -no-output} \
		{}
#
TriggerManager 100 \
	       {TrigMgr -ofile $DMTRIGDIR -ntrig 100 -access TrigMgr_LLO.conf \
	       -streams TrigRStream_LLO.cfg} \
	       {$comnode} \
	       {-when:"mkdir -p /var/spool/dmt-triggers/RDQ_Segments; \
	        waitProc dmt_nameserver 3" \
		-log -no-output:data:trend} \
	       {TrigMgr_LLO.conf TrigRStream_LLO.cfg}
#	       
AlarmManager 100 \
	       {AlarmMgr} \
	       {$comnode} \
	       {-when:"waitProc dmt_nameserver 3" -log -no-output} \
	       {}
#
#---> Set up low-latency iDQ data transfer
link_idq 100   {framelink --delta 1 --stats 300 \
                pull $LOWLATENCY_HOST:32110 $IDQPART} \
             {$htpart} \
             {-no-output -log} \
             {}
#
#=======================================  Redundant h(t) calibration process
# htCalib_L1 400 \
# 	       {gstlal_compute_strain --frame-duration=1 --frames-per-file=1 \
# 	        --config-file=$CALIBCONFIG/gstlal_compute_strain_C00_L1.ini \
# 		--filters-file=$CALIBCONFIG/gstlal_compute_strain_C00_filters_L1.npz} \
# 	       {$htpart} \
# 	       {-no-output:trend:data -log} \
# 	       {}
#
#---> DMTGate generation utility
DMTGate_L1 100 \
             {dq-module -inlist dq-stream-LLO_Online.txt \
              L-DQ+Gate_O3R.json} \
             {$htpart} \
             {-no-output} \
             {dq-stream-LLO_Online.txt L-DQ+Gate_O3R.json}
#
#---> Gated strain generation process
DMTDQ_L1 100  {dq-module -inlists dq-gating-LLO.txt L-DQmod_O3R.json} \
		{$htpart} \
		{-no-output} \
		{dq-gating-LLO.txt \
		 dq-stream-LLO_hoft.txt \
		 dq-stream-LLO_idq.txt \
		 dq-stream-LLO_gate.txt \
		 L-DQmod_O3R.json}
#
#---> Write h(t) data to the gds file system.
L1_ht_Frames 100 \
	    {Frame_Log --delta 1 --partition $DQHOFTPART --filename \
	     "$DMTFRAMEDIR/hoft/L1/.temp/L-L1_DMT_C00-%g-1.gwf"} \
	    {$htpart} \
	    {-no-output -log} \
	    {}
#
#---> Rename frames to reflect true data length
L1_rename_Frames 100 \
	    {$HOME/scripts/name-check L-L1_DMT_C00 "$DMTFRAMEDIR/hoft/L1/.temp" \
	    			  "$DMTFRAMEDIR/hoft/L1/"} \
	    {$htpart} \
	    {-no-output -log} \
	    {}
#
#---> Science Segment generation
Science_RSegs_L1 125 \
		{SegGener -osc hoft_dq_flags.osc -conf Science_Segs_L1.cnf \
			  -partition $DQHOFTPART +seg} \
		{$htpart} \
		{-no-output:trend:data} \
		{hoft_dq_flags.osc,Science_Segs_L1.cnf}
#
#=======================================  Monitoring process definitions
#
#
#---> kleineWelle h(t) Glitch detection
#     Note that this produces data to /dmt/triggers/
#kleineWelle_RHOFT_L1 500 \
#		  {kleineWelleM $HOME/pars/L-ER13_KW_RHOFT.cfg \
#		  		-inlist $HOME/pars/htpart_in.txt} \
#	    	  {$htpart} \
#		  {-log -cd:$DMTRIGDIR/L-KW_RHOFT -no-output:trend:data} \
#		  {L-ER13_KW_RHOFT.cfg htpart_in.txt}
#
#   NS Binary inspiral sensitivity
#
SenseMonitor_HOFT_L1 125 \
		{SenseMonitor -config SenseMonitor_hoft_L1.conf \
                	      -partition $DQHOFTPART} \
                {$htpart} \
                {-no-output:data} \
                {SenseMonitor_hoft_L1.conf}
#
