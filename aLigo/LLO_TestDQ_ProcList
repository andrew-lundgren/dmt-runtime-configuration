#
#    File:	LLO_TestDQ_ProcList
#
#    Purpose:	This ProcList file is used by the procsetup utility to 
#		create the node-specific configuration files for the
#		process manager.
#
#    Author:	J. Zweizig
#
#    Version:	1.0; Modified January 15, 2015
#    		Version for ER7 running before aLigo
#    		2.2; modified December 6, 2018
#    		Draft ER13 version. With gating.
#    		2.3; modified February 12, 2019
#    		Draft ER14/O3 version. With gating.
#
#-
#
#    This file contains one configuration line for each process to be 
#    run. The configuration line may be continued over multiple text 
#    by ending a non-terminal line with a backslash (\). Comments start
#    with a hash-mark (#) and continue to the end of the line. The 
#    configuration line contains the following fields:
#
#    1) Process name
#    2) CPU usage
#    3) Command to be executed by the process
#    4) Node assignment constraints
#    5) Procmgr tag
#    6) Files used by the process
#
#    Each field should be enclosed in braces if it contains white-space 
#    characters. The command line may contain symbols or escape sequences
#    as allowed by the process manager. The "node assignment constraints" 
#    field is a perl expression that evaluates to true if the process may
#    be run on a given node. The symbols in this expression are substituted 
#    with values defined in the node list on a node-by-node basis.
#
#    Generate procmgt configure files from this file by running the 
#    following command:
#
#       procsetup --file:LLO_TestDQ
#
#==========================================================================
#
#=======================================  Infrastructure process definitions
#
#   Servers
#   Load unmeasured
#
NameServer_T 100	{dmt_nameserver } \
	  	{$comnode} \
	  	{-log -no-output} \
		{}
#
TriggerManager_T 100 \
	       {TrigMgr -ofile $DMTRIGDIR -ntrig 100 -access TrigMgr_LLO.conf \
	       -streams TrigTStream_LLO.cfg} \
	       {$comnode} \
	       {-when:"mkdir -p /var/spool/dmt-triggers/DQ_Segments; \
	        waitProc dmt_nameserver 3" \
		-log -no-output:data:trend} \
	       {TrigMgr_LLO.conf TrigTStream_LLO.cfg}
#	       
AlarmManager_T 100 \
	       {AlarmMgr} \
	       {$comnode} \
	       {-when:"waitProc dmt_nameserver 3" -log -no-output} \
	       {}
#
#---> Set up low-latency iDQ data transfer
link_idq_T 100 \
	   {framelink --delta 1 --stats 300 \
	    pull $LOWLATENCY_HOST:32111 $IDQPART} \
	    {$htpart} \
	    {-no-output -log} \
	    {}
#
#=======================================  Test h(t) calibration process
# htCalib_L1_T 5600 \
# 		{gstlal_compute_strain --frame-duration=1 --frames-per-file=1 \
# 		 --config-file=$CALIBCONFIG/gstlal_compute_strain_C00_test_L1.ini \
# 		 --filters-file=$CALIBCONFIG/gstlal_compute_strain_C00_filters_L1.npz} \
# 		{$htpart} \
# 		{-no-output:trend:data -log} \
# 		{}
#
#---> DMTGate generation utility
DMTGate_L1_T 100 \
	     {dq-module -inlist dq-stream-LLO_Online.txt \
              L-DQ+Gate_O3T.json} \
             {$htpart} \
             {-no-output} \
             {dq-stream-LLO_Online.txt L-DQ+Gate_O3T.json}
#
#---> DMTDQ generation utility
DMTDQ_L1_T 100	{dq-module -inlists dq-gating-LLO.txt L-DQmod_O3T.json} \
		{$htpart} \
		{-no-output} \
		{dq-gating-LLO.txt dq-stream-LLO_hoft.txt \
		 dq-stream-LLO_gate.txt dq-stream-LLO_idq.txt \
		 L-DQmod_O3T.json}
#
#---> Transmit to kafka
#L1_ht_Kafka_T 100 \
#	    {framelink --delta 1 --stats 300 \
#		       --verbose $HOME/logs/link_hoft_Kafka-verbose.log \
#		       push $DQHOFTPART $LOWLATENCY_HOST:32108} \
#               {$htpart} \
#               {-no-output -log} \
#               {}
#
#---> Write h(t) data to the gds file system.
L1_ht_Frames_T 100 \
	    {Frame_Log --delta 1 --partition $DQHOFTPART --filename \
	    	       "$DMTFRAMEDIR/hoft/L1/.temp/L-L1_DMT_TEST-%g-1.gwf"} \
	    {$htpart} \
	    {-no-output -log} \
	    {}
#
#---> Rename frames to reflect true data length
L1_rename_Frames_T 100 \
	    {$HOME/scripts/name-check L-L1_DMT_TEST "$DMTFRAMEDIR/hoft/L1/.temp" \
	    			  "$DMTFRAMEDIR/hoft/L1/"} \
	    {$htpart} \
	    {-no-output -log} \
	    {}
#
#---> Science Segment generation
Science_Segs_L1_T 125 \
		{SegGener -osc hoft_dq_flags_T.osc -conf Science_Segs_L1.cnf \
			  -partition $DQHOFTPART +seg} \
		{$htpart} \
		{-no-output:trend:data} \
		{hoft_dq_flags_T.osc Science_Segs_L1.cnf}
#
#---> h(t) frame trends
trender_L1_T 125 \
		{trender  -n L-L1_DMT_TEST_M -i L1 -l dq-stream-LLO_dmtdq.txt} \
		{$htpart} \
		{-no-output:data:html \
		 -env=DMTRENDOUT=$DMTRENDIR/$DMTPROCESS/L-L1_DMT_TEST_M-%6r} \
		{}
#
#=======================================  Monitoring process definitions
#
#
#---> kleineWelle h(t) Glitch detection
#     Note that this produces data to /dmt/triggers/
#kleineWelle_HOFT_L1_T 500 \
#		  {kleineWelleM $HOME/pars/L-ER13_KW_HOFT_T.cfg \
#		  		-inlist $HOME/pars/htpart_in.txt} \
#	    	  {$htpart} \
#		  {-log -cd:$DMTRIGDIR/L-KW_HOFT_T -no-output:trend:data} \
#		  {L-ER13_KW_HOFT_T.cfg htpart_in.txt}
#
#---> Omega on calibrated h(t) data
#
Omega_HOFT_L1_T 300 \
	      {dmt_wstream L1-HOFT_OMEGA_TEST.cfg L1-hoftStreamList.txt \
	      		   $DMTRIGDIR/L-HOFT_Omega/%5g 0} \
	      {$htpart} \
	      {-no-output:trend:data -log} \
	      {L1-HOFT_OMEGA_TEST.cfg L1-hoftStreamList.txt \
	       dq-stream-LLO_hoft.txt dq-stream-LLO_hoft.txt}
#
#   NS Binary inspiral sensitivity
#
SenseMonitor_HOFT_L1_T 125 \
		{SenseMonitor -config SenseMonitor_hoft_L1_T.conf \
                	      -partition $DQHOFTPART} \
                {$htpart} \
                {-no-output:data} \
                {SenseMonitor_hoft_L1_T.conf hoft_dq_flags_T.osc \
		 ReferenceCalibration_hoft_L1_T.xml}
#
