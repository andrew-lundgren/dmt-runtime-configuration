#! /bin/sh
#
#   Arguments
#   p1 process name
#   p2 process arg
#   p3 partition name
#   p4 frame length
#   p5 stat directory
#
#   fproc2 macro finds the pid of a process running the specified executable
#   and having the specified argument.
#   p1 executable name
#   p2 argument text
fproc2() {
    for pid in `pgrep -u $EXEUSER $1`; do
	if [ `ps -fp $pid | grep -c "$2"` != 0 ]; then
	    echo $pid
	    exit 0
	fi
    done
    echo 0
}
#
#   Write status json file
write_status() {
    sed -e "s,GPSTIME,$now_gps,g" \
        -e "s,UNIXTIME,$now_time,g" \
	-e "s,PARTNAME,$part,g" \
        -e "s,PROCNAME,$proc,g" \
	-e "s,DIFFCNT,$diff,g" \
	-e "s,EXPCNT,$expected,g" \
	-e "s,LASTID,$new_gps,g" \
	-e "s,LATENCY,$late,g" \
        $1 \
    >   $stat_name
}
#
#   set the parameters
#
proc=$1
args="$2"
part=$3
frlen=$4
stat_dir=$5
#
[ -n "$EXEUSER" ] || export EXEUSER=$USER
xscript="$0"
scripdir="${xscript%/*}"
node=`uname -n`
#
stat_name="$stat_dir/part-${node}-${part}.json"
now_gps=`when 0 %s`
now_time=`date +%s`
#
#  --  Check that the fill process is running
src_pid=`fproc2 $proc $args`

if [ $src_pid = 0 ]; then
    write_status $scripdir/no-fill-process.json
    exit 2
fi
#
# -- Check that the parition exists
if [ `smstat exists $part` = 0 ]; then
    write_status $scripdir/no-partition.json
    exit 2
fi
#
# -- get the old and new data
statdir="/tmp/llstatus"
[ -d $statdir ] || mkdir -p $statdir
statfile="$statdir/smsource_stats_${part}.txt"
#
#  Get the previous timestamp and buffer count
if [ -f $statfile ]; then
    read prev_gps prev_count < $statfile
else
    prev_gps=0
    prev_count=0
fi
expire=`expr $prev_gps + 600`
#
#  get new gps and count
new_stamp=`when 0 %s`
new_stats="`smstat last_ID buffer_tot $part | tr '\n' ' '`"
new_gps=`echo "$new_stats" | cut -d ' ' -f 1`
new_count=`echo "$new_stats" | cut -d ' ' -f 2`
#
#  write the new stat file.
tmpseq=`when 0 %n`
tmpfile="$statdir/smsource_stats_${part}_${tmpseq}.txt"
echo "$new_stats" > $tmpfile
mv $tmpfile $statfile
#
# figure out the status...
diff=`expr $new_count - $prev_count`
if [ $new_gps -gt $expire -o $diff -lt 0 ]; then
    write_status $scripdir/part-stats-too-old.json
    exit 1
elif [ $new_count = $prev_count ]; then
    write_status  $scripdir/no-new-in-part.json
    exit 2
fi

#--------------------------------------- CHeck that @# of frames is as expected
expected=`expr \( $new_gps - $prev_gps \) / $frlen`
if [ $diff -lt $expected ]; then
    write_status $scripdir/too-few-in-part.json
    exit 2
fi

late=`expr $new_stamp - $new_gps`
if [ $late -ge 15 ]; then
    write_status $scripdir/too-late-in-part.json
    exit 2
fi

#---------------------------------------  Everything is just peachy!
write_status $scripdir/partition-ok.json
exit 0
