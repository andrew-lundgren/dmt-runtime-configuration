#
#  Insert an html header.
1i<!DOCTYPE doctype PUBLIC "-//w3c//dtd html 4.0 transitional//en">\
<html>\
<head>\
  <meta http-equiv="Content-Type"\
 content="text/html; charset=iso-8859-1">\
  <meta name="Author" content="John Zweizig/LIGO">\
  <title>GDS DMT top list</title>\
  <meta http-equiv="Pragma" content="no-cache">\
</head>\
<body>
#
# Convert all the blank space to nbsp's
# 1,/^ *$/{
1, 5 {
s, ,\&nbsp;,g
s,$,<br>,1
}
#
#  build the data table
/ PID /i \
<br> \
<table cellpadding=1>
#
/ PID /, $ {
s,  *, ,g
s,^ *,<tr><td>,1
s, ,</td><td>,g
s,<td>,<td align="right">,g
s,right">\([-+a-z_A-Z0-9]*\)$,left">\1,g
s,$,</td></tr>,1
}

/>PID</s,td,th,g

$a</table> \
</body>

