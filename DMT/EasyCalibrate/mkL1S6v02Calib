#! /bin/sh -x
#
#  This script makes the reference calibration file for SenseMon, GainMon
#  etc. (ReferenceCalibrationDarmErr_L1.xml).
#
#  The DARM_ERR sensing and open loop gain transfer functions are from the 
#  calibration cvs repository in:
# 
#  calibration/frequencydomain/runs/S6/L1/release/V2
#
#  The cal-line darm anmplitude (CalLineAmplASQ) parameter is picked from 
#  the SenseMonitor Log at the time of the OLG scan (GPS 942622860).
#
cat > S6v01-header.xml <<EOF
<?xml version="1.0"?>
<!DOCTYPE LIGO_LW SYSTEM "http://ldas-sw.ligo.caltech.edu/doc/ligolwAPI/html/ligolw_dtd.txt">
<LIGO_LW>
  <LIGO_LW>
    <Param Name="Channel">L1:LSC-DARM_ERR</Param>
    <Param Name="Comment">L1 DARM_ERR calibration, official S6V2 release by 
                          Keita Kawabe 30 November 2009</Param>
    <Time Name="StartTime" Type="GPS">941368015</Time>
    <Param Name="Duration" Type="double">9999999</Param>
    <Time Name="CalibrationTime" Type="GPS">942622874</Time>
    <Param Name="EXCChannel">L1:LSC-DARM_CTRL_EXC_DAQ</Param>
    <Param Name="CalLineFreq" Type="double">1151.5</Param>
    <Param Name="CalLineAmplASQ" Type="double">7.74e-6</Param>
    <Param Name="CalLineAmplEXC" Type="double">0.5</Param>
    <Table Name="DARMChannels">
      <Column Name="ChannelName" Type="string"/>
      <Column Name="RefValue" Type="float"/>
      <Stream Type="Local" Delimiter=" ">
        "L1:LSC-DARM_GAIN" -3.0
      </Stream>
    </Table>
  </LIGO_LW>
</LIGO_LW>
EOF
mkcalibfile -c L1:LSC-DARM_ERR -x S6v01-header.xml -fmax 4000 \
	-olg L-L1_CAL_REF_OLOOP_GAIN_S6_V2-941997600-999999999.txt \
	-cav L-L1_CAL_REF_CAV_GAIN_DARM_ERR_SP_S6_V2-941997600-999999999.txt \
	-o ReferenceCalibrationDarmErr_L1.xml
#
