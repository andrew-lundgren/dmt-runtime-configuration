#! /bin/sh
htput() {
    echo "$1" >>$temp_file
}
file_and_date() {
    if echo $1 | grep -q -e '^\./' -e '^/'; then
       file=$1
    else
       file="`pwd`/$1"
    fi
    date -r $file +"$file (modified: %D %T %Z)"
}
procname=$1
exec_file=$2
arg_list=$3
temp_file="/tmp/$procname-page.html"
restart_list="$DMTHTMLOUT/$procname-restarts.txt"
touch $restart_list
shift 3
htput '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN"'
htput "<HTML><HEAD><TITLE>$procname History</TITLE><meta http-equiv=\"Content-Type\""
htput 'content="text/html; charset=iso-8859-1"></HEAD>'
htput "<BODY BGCOLOR=white><blockquote>"
htput "<center><font size=+1>$procname Restart History</font></center></blockquote>"
startGPS=`when 0 %s`
timeFmt="%Y.%02m.%02d-%02H:%02N:%02S %Z (GPS %s)"
htput "<table border>"
htput "<tr><td>Start time</td><td>`when $startGPS "$timeFmt"`</td></tr>"
htput "<tr><td>Executable</td><td>`file_and_date $exec_file`</td></tr>"
htput "<tr><td>Arguments</td><td>$arg_list</td></tr>"
#
#  --- listed config files
while [ $# != 0 ]; do
    file_name=`echo $1 | sed -e 's,=.*$,,g'`
    file_path=`echo $1 | sed -e 's,^.*=,,g'`
    [ "$file_name" = "$file_path" ] && file_name="config"
    htput "<tr><td>${file_name}</td><td>`file_and_date $file_path`</td></tr>"
    shift
done
htput "</table>"
htput "<br>"
#
# --- List last 10 entries in restart table
htput "<center><font size=+1>Last 10 instances</font></center></blockquote>"
htput "<tr><th>Start Time</th></tr>"
htput "<table>"
htput "`tail -10 $restart_list | sed -e 's,@DATE,<tr><td>,g' -e 's,@END,</td></tr>,g'`"
htput "</table>"
#
# --- end of file
htput "</BODY></HTML>"
echo "@DATE$startGPS@END">> $restart_list
mv $temp_file $DMTHTMLOUT/index.html
